# Predictive-Maintenance-ML


# Machine Failure Classification Project

Bu proje, AI4I 2020 Predictive Maintenance Dataset üzerinde sınıflandırma tahmini yapmayı amaçlamaktadır. Amaç, makine arızalarını önceden tahmin ederek bakım süreçlerini optimize etmektir.

## Başlarken

Bu bölüm, projenin nasıl yerel bir makinede kurulacağı ve çalıştırılacağı ile ilgili talimatları içerir.

### Önkoşullar

Projeyi çalıştırmak için gerekenler:

- Python 3.8+
- Pandas
- Scikit-learn
- NumPy
- Matplotlib (isteğe bağlı, EDA için)

### Kurulum

Projeyi yerel makinenize nasıl kuracağınız:

1. GitLab reposundan projeyi klonlayın.
```bash
git clone <repo-url>
```

2. Gerekli Python kütüphanelerini kurun.
```bash
pip install pandas scikit-learn numpy matplotlib
```

## Kullanım

Projeyi nasıl kullanacağınız:

```bash
python src/predictive_modeling.py
```

## Model Hakkında

Bu projede, makine arızalarını tahmin etmek için 7 farklı makine öğrenmesi modeli kullanılmıştır. Modellerin performansı, bir pipeline üzerinden değerlendirilmiştir. Kullanılan modeller:

- Lojistik Regresyon
- Karar Ağaçları
- Rastgele Orman
- Destek Vektör Makineleri
- K-En Yakın Komşu
- Gradient Boosting
- XGBoost

Her modelin tahmin başarısı, çapraz doğrulama yöntemi ile değerlendirilmiştir.
