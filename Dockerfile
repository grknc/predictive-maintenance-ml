# Python resmi base image'ını kullan
FROM python:3.8

# Çalışma dizinini ayarla
WORKDIR /app

# Bağımlılıkları yükle
COPY requirements.txt .

# Sistem bağımlılıklarını yükle (örneğin Git veya diğer gerekli araçlar)
RUN pip install --no-cache-dir -r requirements.txt


# Uygulamayı çalıştır
CMD ["python", "./src/predictive-model.py"]
